[![](https://framasoft.org/nav/img/icons/ati/sites/git.png)](https://framagit.org)

🇬🇧 **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

🇫🇷 **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

Framapiaf est un service en ligne de miccroblog que Framasoft propose sur le site : https://framapiaf.org

Il repose sur le logiciel [Mastodon](https://joinmastodon.org/).

Ce dépôt ne contient pas Mastodon, juste la page d’accueil.
* * *
